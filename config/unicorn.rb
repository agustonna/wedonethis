worker_processes 3
timeout 15
preload_app true


before_fork do |server, worker|
  Ohm.redis.quit
end

after_fork do |server, worker|
  Ohm.connect(url: ENV["REDISTOGO_URL"])
end