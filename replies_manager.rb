require File.expand_path("./environment", File.dirname(__FILE__))

Dir["./models/**/*.rb"].each  { |rb| require rb }
Dir["./lib/**/*.rb"].each  { |rb| require rb }


Cuba.define do
  on post do
    on "reply" do
      on param("sender"), param("stripped-text") do |sender, body|
        user = User.find(email: sender).to_a.first
        Reply.create(body: body, user: user) if user
      end
    end
  end
end