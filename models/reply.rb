class Reply < Ohm::Model

  include Ohm::Timestamps
  include Ohm::DataTypes

  attribute :body
  attribute :date_sent, Type::Time
  reference :user, :User

end 