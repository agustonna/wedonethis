class User < Ohm::Model

  attribute :name
  attribute :email
  unique :email
  collection :replies, :Reply

  index :name
  index :email
end 