require 'bundler'
Bundler.require(:default)
require 'erb'
require 'date'
require "logger"
Ohm.connect(url: ENV["REDISTOGO_URL"])