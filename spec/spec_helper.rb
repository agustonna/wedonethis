require 'bundler'
Bundler.require(:default, :test)
Dotenv.load
require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/pride'
require 'malone/test'



Ohm.connect(url: ENV["REDIS_URL"])

Dir["./models/**/*.rb"].each  { |rb| require rb }


class MiniTest::Spec
  after :each do
    Ohm.flush
    Malone.deliveries.clear
  end
end
