require File.expand_path("./spec_helper", File.dirname(__FILE__))
require "rack/test"
require './replies_manager'

class TestCuba < Minitest::Test
  include Rack::Test::Methods
  
  def app
    Cuba
  end

  def setup
    @user1 = User.create(name: "test", email: "one@charango.com")
  end

  def test_app_should_create_an_email_on_reply
    post "/reply", {"sender" => @user1.email, "stripped-text" => "cualquier mensaje"} 
    refute_empty Reply.all.to_a #TODO -> MEJORAR EL ASSERTION
  end

  def teardown
    Ohm.flush
  end
end