require File.expand_path("./spec_helper", File.dirname(__FILE__))
require 'digest_creator'

describe DigestCreator do
  before do
    @user1 = User.create(name: "teest", email: "onee@charango.com")
    @user2 = User.create(name: "teest 2", email: "twoo@charango.com")
    @reply1 = Reply.create(body: "lo que sea", user: @user1)
    @reply2 = Reply.create(body: "lo que sea 2", user: @user2)
    
    @replies = [@reply1, @reply2]
    @response = DigestCreator.new(@replies).formatted_digest
  end

  it "should return a string" do
    @response.must_be_instance_of String
  end

  it "should format the outgoing email" do
    @replies.each do |reply|
      @response.must_match(reply.body)
    end
  end

end