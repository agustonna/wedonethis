require File.expand_path("./spec_helper", File.dirname(__FILE__))
require 'mailer'
require 'digest_creator'

describe Mailer do
  before do
    @user1 = User.create(name: "test", email: "one@charango.com")
    @user2 = User.create(name: "test 2", email: "two@charango.com")
    @reply1 = Reply.create(body: "lo que sea", user: @user1)
    @reply2 = Reply.create(body: "lo que sea 2", user: @user2)
    
    @replies = [@reply1, @reply2]
    @digest = DigestCreator.new(@replies).formatted_digest
  end

  it "should have users" do
    User.all.to_a.wont_be_empty
  end

  it "must send questioning email to every recipient" do
    Mailer.new.send_questioning_email
    Malone.deliveries.size.must_equal User.all.size
  end

  it "must email the team with the digest" do
    Mailer.new.send_daily_digest(@replies)
    Malone.deliveries.size.must_equal 1
  end
end