require File.expand_path("./../environment", File.dirname(__FILE__))

class Mailer

  def initialize

=begin
#PARA DEVELOPMENT ONLY
@malone = Malone.connect(url: "smtp://127.0.0.1:1025",
                             domain: "dreepi.com")
=end

    @malone = Malone.connect(port: ENV["MAILGUN_SMTP_PORT"],
                             host: ENV["MAILGUN_SMTP_SERVER"],
                             user: ENV["MAILGUN_SMTP_LOGIN"],
                             password: ENV["MAILGUN_SMTP_PASSWORD"],
                             domain: ENV["MAILGUN_DOMAIN"])
  end
  
  def send_questioning_email
    User.all.to_a.each do |user|
      @malone.deliver(from: "hugo@dreepi.com",
                     to: user.email,
                     subject: "¿Que hiciste hoy?",
                     text: "#{user.name} contale a Hugo que hiciste hoy respondiendo este email",
                     replyto: ENV["MALONE_REPLYTO_DOMAIN"])
    end
  end

  def send_daily_digest(replies)

    filtered_nil_replies = filter_nil_replies(replies)
    filtered_replies = filter_old_replies(filtered_nil_replies)

    digest = DigestCreator.new(filtered_replies).formatted_digest
    @malone.deliver(from: "hugo@dreepi.com",
                   to: "team@dreepi.com",
                   subject: "Que paso ayer",
                   html: "#{digest}" ) unless filtered_replies.size == 0
  end

  private 


  def filter_nil_replies(replies)
    replies.delete_if {|reply| reply == nil}
  end
  #Este metodo se aseguro de no mandar ninguna reply
  # mas vieja que la fecha anterior al dia que se envia.
  def filter_old_replies(replies)
    replies.keep_if {|reply| DateTime.strptime(reply.created_at.to_s, '%s').to_date == Date.today.prev_day}
  end
end