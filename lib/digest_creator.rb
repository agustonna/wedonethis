require File.expand_path("./../environment", File.dirname(__FILE__))


class DigestCreator

include ERB::Util

  def initialize(replies)
    @replies = replies
  end

  def formatted_digest
    mail_template = './digested_mail.html.erb'
    template = ERB.new(File.read(mail_template))
    template.result(binding)
  end
end